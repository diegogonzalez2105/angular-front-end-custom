import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { AppComponent } from './app.component';
import { ButtonComponent } from './components/common/button/button.component';
import { LiComponent } from './components/common/li/li.component';
import { HomeComponent } from './components/pages/home/home.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    LiComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    MatSlideToggleModule,
    NgbModule,
    SweetAlert2Module,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
