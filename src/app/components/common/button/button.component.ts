import { Component,Input  } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
  @Input() Id: String = '';
  @Input() Alt: String = '';
  @Input() Src: String = '';
  @Input() name: String = '';
  @Input() class: String = '';
  @Input() value: String = '';
  @Input() type: String = '';
  @Input() placeholder: String = '';
  @Input() Class: String = '';
  constructor() { }

  ngOnInit(): void {
  }

}
