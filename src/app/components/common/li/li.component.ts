import { Component,Input  } from '@angular/core';
import { Main } from './../../../models/main';

@Component({
  selector: 'app-li',
  templateUrl: './li.component.html',
  styleUrls: ['./li.component.scss']
})
export class LiComponent {
  @Input() Class: String = '';
  @Input() items: Array<Main> = [];


}
