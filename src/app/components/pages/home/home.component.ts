import { Component } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ModalDismissReasons, NgbDatepickerModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BancoService } from './../../../services/banco.service';
import { bancoIist, tipoc, findsaldo } from './../../../models/banco';
import { Nuebac, ConsigI, RerirarI } from './../../../models/banco';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],

})
export class HomeComponent {
  itemLisu: Array<bancoIist> = [];
  itemTipoc: Array<tipoc> = [];
  itemSaldo: Array<findsaldo> = [];
  nNombre: string = "";
  Newsa: string = "";
  typo: string = "";
  NewsaldoC: string = "";

  NewsaldoR: string = "";
  closeResult = '';
  closeResulttow = '';






  constructor(private BancoService: BancoService, private modalService: NgbModal) {
  }

  /* Modal con parametro del usuario de la cuenta */

  open(content: any, id: any) {
    console.log(id);
    this.BancoService.findSaldoc(id).subscribe((data) => {
      if (data.status == 1) {

        this.itemSaldo = data.data;
        console.log(this.itemSaldo, "saldo");
      } else {
        this.notification(false, data.msg);
      }
    }, (error) => {
      if (error.status != 0) {
        this.notification(false, 'error de conexion');
      } else {
        this.notification(false, error.error.msg);
      }
    });


    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }




  opendos(contenttow: any, id: any) {
    this.BancoService.findSaldoc(id).subscribe((data) => {
      if (data.status == 1) {

        this.itemSaldo = data.data;
        console.log(this.itemSaldo, "saldo");
      } else {
        this.notification(false, data.msg);
      }
    }, (error) => {
      if (error.status != 0) {
        this.notification(false, 'error de conexion');
      } else {
        this.notification(false, error.error.msg);
      }
    });


    this.modalService.open(contenttow, { ariaLabelledBy: 'modal-basic-titledo' }).result.then(
      (result) => {
        this.closeResulttow = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResulttow = `Dismissed ${this.getDismissReason(reason)}`;
      },
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }



  ngOnInit(): void {
    /* carga lista de usuario con saldo y cuenta */
    this.BancoService.findlistau().subscribe((data) => {
      if (data.status == 1) {

        this.itemLisu = data.data;
        console.log(this.itemLisu, "list");


      } else {
        this.notification(false, data.msg);
      }
    }, (error) => {
      if (error.status != 0) {
        this.notification(false, 'error de conexion');
      } else {
        this.notification(false, error.error.msg);
      }
    });


    /* carga lista tipo cuenta  */
    this.BancoService.ListaTipoCuenta().subscribe((data) => {
      if (data.status == 1) {

        this.itemTipoc = data.data;
        console.log(this.itemTipoc, "list");


      } else {
        this.notification(false, data.msg);
      }
    }, (error) => {
      if (error.status != 0) {
        this.notification(false, 'error de conexion');
      } else {
        this.notification(false, error.error.msg);
      }
    });
  }

  /* crea nuevo usuario con numero de cuenta  */
  btnEnviar(): void {
    console.log("Click");
    {
      const itemContac: Nuebac = {
        nombreU: this.nNombre,
        Newsaldo: this.Newsa,
        tipo: this.typo,
      }
      console.log(itemContac);

      if ((this.nNombre =="")||(this.Newsa =="")||(this.typo =="")) {

        alert("Todos los  campo obligatorio")

      }
      this.BancoService.aggregateContac(itemContac).subscribe(
        (response) => {
          console.log(response.status, "SIRESPO");
          if (response.status == 1) {
            alert(response.msg,);
            setTimeout("location.href='http://localhost:4200/'");
          } else {
            alert(response.msg,);

          }
        }, (error) => {
          if (error.status == 0) {

            alert('error de conexion');
          } else {
            alert(error.error.msg);
          }

        });
    }
  }

  /* Realizar consingacion */
  btnConsignar(id: any) {
    console.log("Click");
    {
      const itemC: ConsigI = {
        id: id,
        NewsaldoC: this.NewsaldoC
      }
      console.log(itemC);
      this.BancoService.consignar(itemC).subscribe(
        (response) => {
          console.log(response.status, "SIRESPO");
          if (response.status == 1) {
            alert(response.msg,);
            setTimeout("location.href='http://localhost:4200/'");
          } else {
            alert(response.msg,);
          }
        }, (error) => {
          if (error.status == 0) {

            alert('error de conexion');
          } else {
            alert(error.error.msg);
          }

        });
    }

  }

 /* funcion para retirar dinero */
  btnRetirar(id: any) {
    console.log("Click");
    {
      const itemR: RerirarI = {
        id: id,
        NewsaldoC: this.NewsaldoR
      }
      console.log(itemR);
      this.BancoService.retirar(itemR).subscribe(
        (response) => {
          console.log(response.status, "SIRESPO");
          if (response.status == 1) {
            alert(response.msg,);
            setTimeout("location.href='http://localhost:4200/'");
          } else {
            alert(response.msg,);
          }
        }, (error) => {
          if (error.status == 0) {

            alert('error de conexion');
          } else {
            alert(error.error.msg);
          }

        });
    }

  }



  notification(type: boolean, text: string) {
    if (type) {
      Swal.fire({
        icon: 'success',
        text: text
      });
    } else {
      Swal.fire({
        icon: 'error',
        text: text
      });
    }

  }




}
