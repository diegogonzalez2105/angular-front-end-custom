import { Injectable } from '@angular/core';
import { HttpClient,HttpEvent,HttpRequest } from '@angular/common/http';
import { Response } from './../models/response';
import { environment } from './../../environments/environment';
import { Nuebac,ConsigI,RerirarI } from './../models/banco';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BancoService {
  private urllavarvel = environment.baseUrl;

  constructor(private http: HttpClient) { }


  findlistau() {
    return this.http.get<Response>(this.urllavarvel + 'CuentaUsuario');
  }

  ListaTipoCuenta() {
    return this.http.get<Response>(this.urllavarvel + 'tipoCuenta');
  }

  findSaldoc(id: string) {

    return this.http.post<Response>(this.urllavarvel + 'Findsaldo',{ id:id });
  }



  aggregateContac(item: Nuebac) {
    return this.http.post<Response>(this.urllavarvel + 'Nuwcuenta', item);
  }

  consignar(item: ConsigI) {
    return this.http.post<Response>(this.urllavarvel + 'ActulizarC', item);
  }


  retirar(item: RerirarI) {
    return this.http.post<Response>(this.urllavarvel + 'RetiroC', item);
  }






}
