export interface Response {
  data?: any
  msg: string;
  status: number;
}
